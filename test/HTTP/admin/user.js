"use strict";

module.exports = function (app, g) {

    var should = require('should'),
        Users = require('mongoose').model('User');

    describe('GET /admin/users ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/users')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/users')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/users')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

    describe('POST /admin/users ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/admin/users')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .post('/admin/users')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна создать задачу и дать 302 админу', function (done) {

            var name = 'Новое имя для теста';

            g.orangeAdmin
                .post('/admin/users')
                .field('lname', 'lname')
                .field('fname', 'fname')
                .field('email', 'testemail@ya.ru')
                .field('admin', 'true')
                .field('active', 'true')
                .field('pass', 'qwe123')
                .set('X-Requested-With', 'XMLHttpRequest')
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/users');

                    Users.count({ email: 'testemail@ya.ru' }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

    });

    describe('GET /admin/users/new ', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/users/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/users/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/users/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/users/:id/edit', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/users/' + g.user.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/users/' + g.user.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/users/' + g.user.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/users/:id/publish', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/users/' + g.user.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/users/' + g.user.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .expect('Location', '/admin/users')
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/users/' + g.user.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/users/:id/unpublish', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/users/' + g.user.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/users/' + g.user.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .expect('Location', '/admin/users')
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/users/' + g.user.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });


    describe('POST /admin/users/:id ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/admin/users/' + g.user.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .post('/admin/users/' + g.user.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна обновить пользователя и дать 302 админу', function (done) {

            var userName = 'Второе новое странное имя';

            g.orangeAdmin
                .post('/admin/users/' + g.user.getId())
                .field('lname', userName)
                .field('fname', g.user.getFname())
                .field('email', g.user.getEmail())
                .field('admin', String(g.user.isAdmin()))
                .field('active', String(g.user.isActive()))
                .field('pass', g.user.getPass())
                .set('X-Requested-With', 'XMLHttpRequest')
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/users');

                    Users.count({ lname: userName }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

    });


};