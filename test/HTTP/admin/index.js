"use strict";

module.exports = function (app, g) {

    var should = require('should');

    describe('GET / ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });


    });

};