"use strict";

module.exports = function (app, g) {

    var should = require('should'),
        Posts = require('mongoose').model('Post');

    describe('GET /admin/posts ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/posts')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/posts')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/posts')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

    describe('POST /admin/posts ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/admin/posts')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .post('/admin/posts')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна создать задачу и дать 302 админу', function (done) {

            var name = 'Новое имя для теста';

            g.orangeAdmin
                .post('/admin/posts')
                .set('X-Requested-With', 'XMLHttpRequest')
                .field('title', name)
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/posts');

                    Posts.count({ title: name }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

    });

    describe('GET /admin/posts/new ', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/posts/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/posts/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/posts/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/posts/:id/edit', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/posts/' + g.post.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/posts/' + g.post.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/posts/' + g.post.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/posts/:id/publish', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/posts/' + g.post.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/posts/' + g.post.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .expect('Location', '/admin/posts')
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/posts/' + g.post.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/posts/:id/unpublish', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/posts/' + g.post.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/posts/' + g.post.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .expect('Location', '/admin/posts')
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/posts/' + g.post.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });


    describe('POST /admin/posts/:id ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/admin/posts/' + g.post.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .post('/admin/posts/' + g.post.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна обновить задачу и дать 302 админу', function (done) {

            var postName = 'Второе новое странное имя';

            g.orangeAdmin
                .post('/admin/posts/' + g.post.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .field('title', postName)
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/posts');

                    Posts.count({ title: postName }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

    });


};