"use strict";

module.exports = function (app, g) {

    var should = require('should'),
        Contests = require('mongoose').model('Contest');

    describe('GET /admin/contests ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

    describe('POST /admin/contests ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/admin/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .post('/admin/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна создать и дать 302 админу', function (done) {

            var name = 'Тестовый контест';

            g.orangeAdmin
                .post('/admin/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .field('name', name)
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/contests');

                    Contests.count({ name: name }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

    });

//    describe('GET /admin/contests/:id ', function () {
//
//        it('Должна дать 403 пользователю', function (done) {
//            g.orangeUser
//                .get('/admin/contests/' + g.contest.getId())
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(403)
//                .end(done);
//        });
//
//        it('Должна дать 200 админу', function (done) {
//            g.orangeAdmin
//                .get('/admin/contests/' + g.contest.getId())
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(200)
//                .end(done);
//        });
//
//        it('Должна дать 302 гостю', function (done) {
//            g.orangeGuest
//                .get('/admin/contests/' + g.contest.getId())
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .end(done);
//        });
//
//    });

    describe('GET /admin/contests/new ', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/contests/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/contests/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/contests/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/contests/:id/edit', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/contests/' + g.contest.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/contests/' + g.contest.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/contests/' + g.contest.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

//    describe('GET /admin/contests/:id/publish', function () {
//
//        it('Должна дать 403 пользователю', function (done) {
//            g.orangeUser
//                .get('/admin/contests/' + g.contest.getId() + '/publish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(403)
//                .end(done);
//        });
//
//        it('Должна дать 200 админу', function (done) {
//            g.orangeAdmin
//                .get('/admin/contests/' + g.contest.getId() + '/publish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .expect('Location', '/admin/contests')
//                .end(done);
//        });
//
//        it('Должна дать 302 гостю', function (done) {
//            g.orangeGuest
//                .get('/admin/contests/' + g.contest.getId() + '/publish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .end(done);
//        });
//
//    });

//    describe('GET /admin/contests/:id/unpublish', function () {
//
//        it('Должна дать 403 пользователю', function (done) {
//            g.orangeUser
//                .get('/admin/contests/' + g.contest.getId() + '/unpublish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(403)
//                .end(done);
//        });
//
//        it('Должна дать 200 админу', function (done) {
//            g.orangeAdmin
//                .get('/admin/contests/' + g.contest.getId() + '/unpublish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .expect('Location', '/admin/contests')
//                .end(done);
//        });
//
//        it('Должна дать 302 гостю', function (done) {
//            g.orangeGuest
//                .get('/admin/contests/' + g.contest.getId() + '/unpublish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .end(done);
//        });
//
//    });


    describe('POST /admin/contests/:id ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/admin/contests/' + g.contest.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .post('/admin/contests/' + g.contest.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна обновить и дать 302 админу', function (done) {

            var contestName = 'Второе новое странное имя контеста';

            g.orangeAdmin
                .post('/admin/contests/' + g.contest.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .field('name', contestName)
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/contests');

                    Contests.count({ name: contestName }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

    });


};