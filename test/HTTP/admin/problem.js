"use strict";

module.exports = function (app, g) {

    var should = require('should'),
        Problems = require('mongoose').model('Problem');

    describe('GET /admin/problems ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/problems')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/problems')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/problems')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

    describe('POST /admin/problems ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/admin/problems')
                .attach('tests', 'test/attach/a+b.zip')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .post('/admin/problems')
                .attach('tests', 'test/attach/a+b.zip')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна создать задачу и дать 302 админу', function (done) {
            g.orangeAdmin
                .post('/admin/problems')
                .set('X-Requested-With', 'XMLHttpRequest')
                .field('name', 'Новое имя тестовой задачи')
                .field('iofilename', 'ab')
                .field('timeLimit', 1)
                .field('memoryLimit', 64)
                .attach('tests', 'test/attach/a+b.zip')
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/problems');

                    Problems.count({ name: 'Новое имя тестовой задачи' }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

        it('Должна создать задачу без теста', function (done) {
            g.orangeAdmin
                .post('/admin/problems')
                .set('X-Requested-With', 'XMLHttpRequest')
                .field('name', 'Новое имя тестовой задачи')
                .field('iofilename', 'ab')
                .field('timeLimit', 1)
                .field('memoryLimit', 64)
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/problems');

                    Problems.count({ name: 'Новое имя тестовой задачи' }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(2);
                        done();
                    });
                });
        });

    });

    describe('GET /admin/problems/new ', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/problems/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/problems/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/problems/new')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/problems/:id/edit', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/problems/' + g.problem.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/problems/' + g.problem.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/problems/' + g.problem.getId() + '/edit')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/problems/:id/publish', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/problems/' + g.problem.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/problems/' + g.problem.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .expect('Location', '/admin/problems')
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/problems/' + g.problem.getId() + '/publish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

    describe('GET /admin/problems/:id/unpublish', function () {

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .get('/admin/problems/' + g.problem.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/admin/problems/' + g.problem.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .expect('Location', '/admin/problems')
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/admin/problems/' + g.problem.getId() + '/unpublish')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });


    describe('POST /admin/problems/:id ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/admin/problems/' + g.problem.getId())
                .attach('tests', 'test/attach/a+b.zip')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 403 пользователю', function (done) {
            g.orangeUser
                .post('/admin/problems/' + g.problem.getId())
                .attach('tests', 'test/attach/a+b.zip')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(403)
                .end(done);
        });

        it('Должна обновить задачу и дать 302 админу', function (done) {

            var problemName = 'Второе новое странное имя задачи';

            g.orangeAdmin
                .post('/admin/problems/' + g.problem.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .field('name', problemName)
                .field('iofilename', 'ab')
                .field('timeLimit', 1)
                .field('memoryLimit', 64)
                .attach('tests', 'test/attach/a+b.zip')
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/problems');

                    Problems.count({ name: problemName }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

        it('Без тестов тоже должна сохраниться', function (done) {

            var problemName = 'Второе новое странное имя задачи';

            g.orangeAdmin
                .post('/admin/problems/' + g.problem.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .field('name', problemName)
                .field('iofilename', 'ab')
                .field('timeLimit', 1)
                .field('memoryLimit', 64)
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    res.headers.location.should.equal('/admin/problems');

                    Problems.count({ name: problemName }, function (err, elem) {
                        should.not.exists(err);
                        elem.should.equal(1);
                        done();
                    });
                });
        });

    });


};