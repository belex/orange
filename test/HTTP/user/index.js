"use strict";

module.exports = function (app, g) {

    var should = require('should');

    describe('GET / ', function () {

        it('Должна дать 200 пользователю', function (done) {
            g.orangeUser
                .get('/')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 гостю', function (done) {
            g.orangeGuest
                .get('/')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

    describe('GET /register ', function () {

        it('Должна дать 200 пользователю', function (done) {
            g.orangeUser
                .get('/register')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/register')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 гостю', function (done) {
            g.orangeGuest
                .get('/register')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

    describe('POST /register ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/register')
                .field('fname', 'Имя')
                .field('lname', 'Фамилия')
                .field('group', 'Группа в универе')
                .field('email', 'ya@ya.ru')
                .field('pass', 'qwe123')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

};