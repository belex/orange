"use strict";

module.exports = function (app, g) {

    var should = require('should'),
        Contests = require('mongoose').model('Contest');

    describe('GET /contests ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 200 пользователю', function (done) {
            g.orangeUser
                .get('/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/contests')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

//    describe('POST /contests ', function () {
//
//        it('Должна дать 302 гостю', function (done) {
//            g.orangeGuest
//                .post('/contests')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .end(done);
//        });
//
//        it('Должна дать 403 пользователю', function (done) {
//            g.orangeUser
//                .post('/contests')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(403)
//                .end(done);
//        });
//
//        it('Должна создать и дать 302 админу', function (done) {
//
//            var name = 'Тестовый контест';
//
//            g.orangeAdmin
//                .post('/contests')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .field('name', name)
//                .end(function (err, res) {
//                    should.not.exists(err);
//                    should.exists(res);
//                    res.headers.location.should.equal('/contests');
//
//                    Contests.count({ name: name }, function (err, elem) {
//                        should.not.exists(err);
//                        elem.should.equal(1);
//                        done();
//                    });
//                });
//        });
//
//    });

//    describe('GET /contests/:id ', function () {
//
//        it('Должна дать 403 пользователю', function (done) {
//            g.orangeUser
//                .get('/contests/' + g.contest.getId())
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(403)
//                .end(done);
//        });
//
//        it('Должна дать 200 админу', function (done) {
//            g.orangeAdmin
//                .get('/contests/' + g.contest.getId())
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(200)
//                .end(done);
//        });
//
//        it('Должна дать 302 гостю', function (done) {
//            g.orangeGuest
//                .get('/contests/' + g.contest.getId())
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .end(done);
//        });
//
//    });

    describe('GET /contests/:id/register', function () {

        it('Должна дать 302 пользователю', function (done) {
            g.orangeUser
                .get('/contests/' + g.contest.getId() + '/register')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/contests/' + g.contest.getId() + '/register')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/contests/' + g.contest.getId() + '/register')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

//    describe('GET /contests/:id/publish', function () {
//
//        it('Должна дать 403 пользователю', function (done) {
//            g.orangeUser
//                .get('/contests/' + g.contest.getId() + '/publish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(403)
//                .end(done);
//        });
//
//        it('Должна дать 200 админу', function (done) {
//            g.orangeAdmin
//                .get('/contests/' + g.contest.getId() + '/publish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .expect('Location', '/contests')
//                .end(done);
//        });
//
//        it('Должна дать 302 гостю', function (done) {
//            g.orangeGuest
//                .get('/contests/' + g.contest.getId() + '/publish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .end(done);
//        });
//
//    });

//    describe('GET /contests/:id/unpublish', function () {
//
//        it('Должна дать 403 пользователю', function (done) {
//            g.orangeUser
//                .get('/contests/' + g.contest.getId() + '/unpublish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(403)
//                .end(done);
//        });
//
//        it('Должна дать 200 админу', function (done) {
//            g.orangeAdmin
//                .get('/contests/' + g.contest.getId() + '/unpublish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .expect('Location', '/contests')
//                .end(done);
//        });
//
//        it('Должна дать 302 гостю', function (done) {
//            g.orangeGuest
//                .get('/contests/' + g.contest.getId() + '/unpublish')
//                .set('X-Requested-With', 'XMLHttpRequest')
//                .expect(302)
//                .end(done);
//        });
//
//    });

};