"use strict";

module.exports = function (app, g) {

    var should = require('should'),
        Attempt = require('mongoose').model('Attempt');

    describe('GET /problems ', function () {

        it('Должна дать 200 пользователю', function (done) {
            g.orangeUser
                .get('/problems')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/problems')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 гостю', function (done) {
            g.orangeGuest
                .get('/problems')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

    describe('GET /problems/:id ', function () {

        it('Должна дать 200 пользователю', function (done) {
            g.orangeUser
                .get('/problems/' + g.problem.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/problems/' + g.problem.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 гостю', function (done) {
            g.orangeGuest
                .get('/problems/' + g.problem.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });

    describe('POST /problems/:id/check ', function () {

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .post('/problems/' + g.problem.getId() + '/check')
                .attach('source', 'test/attach/accepted.cpp')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 302 пользователю', function (done) {
            g.orangeUser
                .post('/problems/' + g.problem.getId() + '/check')
                .attach('source', 'test/attach/accepted.cpp')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

        it('Должна дать 302 админу', function (done) {

            g.orangeAdmin
                .post('/problems/' + g.problem.getId() + '/check')
                .attach('source', 'test/attach/accepted.cpp')
                .set('X-Requested-With', 'XMLHttpRequest')
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    done();
                });
        });

        it('Должна дать 302 админу', function (done) {

            g.orangeAdmin
                .post('/problems/' + g.problem.getId() + '/check')
                .attach('source', 'test/attach/accepted.cpp')
                .set('X-Requested-With', 'XMLHttpRequest')
                .end(function (err, res) {
                    should.not.exists(err);
                    should.exists(res);
                    done();
                });
        });

        it('Должна корректно провериться', function (done) {

            Attempt.count({ problem: g.problem.getId(), ans: 1 }, function (err, count1) {
                should.not.exists(err);

                g.orangeAdmin
                    .post('/problems/' + g.problem.getId() + '/check')
                    .attach('source', 'test/attach/accepted.cpp')
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end(function (err, res) {
                        should.not.exists(err);
                        should.exists(res);

                        Attempt.count({ problem: g.problem.getId(), ans: 1 }, function (err, count2) {
                            should.not.exists(err);

                            console.log(count1, count2);

                            count1.should.be.equal(count2 - 1);

                            done();

                        });

                    });
            });

        });


        it('Должна быть ошибка компиляции', function (done) {

            Attempt.count({ problem: g.problem.getId(), ans: 5 }, function (err, count1) {
                should.not.exists(err);

                g.orangeAdmin
                    .post('/problems/' + g.problem.getId() + '/check')
                    .attach('source', 'test/attach/compilationerror.cpp')
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end(function (err, res) {
                        should.not.exists(err);
                        should.exists(res);

                        Attempt.count({ problem: g.problem.getId(), ans: 5 }, function (err, count2) {
                            should.not.exists(err);

                            console.log(count1, count2);

                            count1.should.be.equal(count2 - 1);

                            done();

                        });

                    });
            });

        });

        it('Должна быть runtime error', function (done) {

            Attempt.count({ problem: g.problem.getId(), ans: 6 }, function (err, count1) {
                should.not.exists(err);

                g.orangeAdmin
                    .post('/problems/' + g.problem.getId() + '/check')
                    .attach('source', 'test/attach/runtimeerror.cpp')
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end(function (err, res) {
                        should.not.exists(err);
                        should.exists(res);

                        Attempt.count({ problem: g.problem.getId(), ans: 6 }, function (err, count2) {
                            should.not.exists(err);

                            console.log(count1, count2);

                            count1.should.be.equal(count2 - 1);

                            done();

                        });

                    });
            });

        });


        it('Должна быть timelimit', function (done) {

            Attempt.count({ problem: g.problem.getId(), ans: 3 }, function (err, count1) {
                should.not.exists(err);

                g.orangeAdmin
                    .post('/problems/' + g.problem.getId() + '/check')
                    .attach('source', 'test/attach/timelimit.cpp')
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end(function (err, res) {
                        should.not.exists(err);
                        should.exists(res);

                        Attempt.count({ problem: g.problem.getId(), ans: 3 }, function (err, count2) {
                            should.not.exists(err);

                            console.log(count1, count2);

                            count1.should.be.equal(count2 - 1);

                            done();

                        });

                    });
            });

        });

        it('Должна быть wronganswer', function (done) {

            Attempt.count({ problem: g.problem.getId(), ans: 2 }, function (err, count1) {
                should.not.exists(err);

                g.orangeAdmin
                    .post('/problems/' + g.problem.getId() + '/check')
                    .attach('source', 'test/attach/wronganswer.cpp')
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end(function (err, res) {
                        should.not.exists(err);
                        should.exists(res);

                        Attempt.count({ problem: g.problem.getId(), ans: 2 }, function (err, count2) {
                            should.not.exists(err);

                            console.log(count1, count2);

                            count1.should.be.equal(count2 - 1);

                            done();

                        });

                    });
            });

        });


    });

};