
"use strict";

var _ = require('underscore'),
    app = require('../server'),
    acl = require('../config/acl'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Problem = mongoose.model('Problem'),
    Attempt = mongoose.model('Attempt'),
    Contest = mongoose.model('Contest'),
    Post = mongoose.model('Post'),
    g = {
        admin: null,
        user: null,
        guest: null,
        problem: null,
        contest: null,
        adminEmail: 'adminuseremail@gmail.com',
        adminPass: 'blogadminuserpass',
        userEmail: 'useruseremail@gmail.com',
        userPass: 'blogadminuserpass',
        clearFunc: function (cb) {
            User.remove(function () {
                Problem.remove(function () {
                    Contest.remove(function () {
                        Post.remove(function () {
                            Attempt.remove(function () {
                                cb();
                            });
                        });
                    });
                });
            });
        },
        createUsers: function (cb) {

            User.create({ email: g.adminEmail, pass: g.adminPass, lname: 'Admin', fname: 'admin' }, function (err, adminUser) {
                if ( err ) { throw err; }

                acl.addUserRoles( adminUser.getId(), 'admin', function(err) {
                    if ( err ) { throw err; }

                    g.admin = adminUser;

                    User.create({ email: g.userEmail, pass: g.userPass, lname: 'User', fname: 'user' }, function (err, user) {
                        if ( err ) { throw err; }

                        g.user = user;

                        acl.addUserRoles( user.getId(), 'user', cb);

                    });

                });

            });

        },
        createProblems: function (cb) {

            Problem.create({ name: 'Problem', iofilename: 'ab', timeLimit: 2, memoryLimit: 64, text: "text", active: true }, function (err, newProblem) {
                if ( err ) { throw err; }

                g.problem = newProblem;
                newProblem.updateTests('test/attach/a+b.zip', cb);

            });

        },
        prepare: function (done) {
            g.clearFunc(function () {
                g.createUsers(function () {
                    g.createProblems(function () {
                        Contest.create({ name: 'Contest', allowRegister: true, problems: [g.problem.getId()], users: [g.user.getId()] }, function (err, contest) {
                            if ( err ) { throw err; }

                            g.contest = contest;

                            Post.create({ title: 'Новая новость', user: g.user.getId(), text: 'Текст' }, function (err, post) {
                                if ( err ) { throw err; }

                                g.post = post;
                                done();

                            });

                        });

                    });

                });

            });

        }
    };

describe('HTTP', function () {

    before(g.prepare);

    require('./HTTP/index')(app, g);

});

describe('Unit', function () {

    before(g.prepare);

    require('./Unit/index')(app, g);

});