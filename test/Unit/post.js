"use strict";

module.exports = function (app, g) {

    var mongoose = require('mongoose'),
        Post = mongoose.model('Post'),
        User = mongoose.model('User'),
        should = require('should');

    describe('#getTitle()', function () {
        it('должен быть', function (done) {
            new Post().should.have.property('getTitle');
            new Post().getTitle.should.be.type('function');
            done();
        });

        it('Должен вернуть значение заголовка', function (done) {
            var newPost = new Post(),
                title = 'title';

            newPost.title = title;
            newPost.getTitle().should.be.equal(title);
            done();
        });
    });

    describe('#setTitle()', function () {
        it('должен быть', function (done) {
            new Post().should.have.property('setTitle');
            new Post().setTitle.should.be.type('function');
            done();
        });

        it('должен установить заголовок', function (done) {
            var newPost = new Post(),
                newTitle = 'newTitle    ';

            newPost.setTitle(newTitle);
            newPost.getTitle().should.be.equal(newTitle.trim());
            done();
        });

    });

    describe('#getText()', function () {
        it('должен быть', function (done) {
            new Post().should.have.property('getText');
            new Post().getText.should.be.type('function');
            done();
        });

        it('Должен вернуть значение тела сообщения', function (done) {
            var newPost = new Post(),
                text = 'new mega text!';

            newPost.text = text;
            newPost.getText().should.be.equal(text);
            done();
        });
    });

    describe('#setText()', function () {
        it('должен быть', function (done) {
            new Post().should.have.property('setText');
            new Post().setText.should.be.type('function');
            done();
        });

        it('должен установить тело сообщения', function (done) {
            var newPost = new Post(),
                newText = 'newText    ';

            newPost.setText(newText);
            newPost.getText().should.be.equal(newText.trim());
            done();
        });
    });

    describe('#getUserId()', function () {
        it('должен быть', function (done) {
            new Post().should.have.property('getUserId');
            new Post().getUserId.should.be.type('function');
            done();
        });

        it('Должен вернуть id пользователя', function (done) {
            var newPost = new Post(),
                userId = new User().getId();

            newPost.user = userId;
            newPost.getUserId().should.be.equal(userId);
            done();
        });
    });

    describe('#setUserId()', function () {
        it('должен быть', function (done) {
            new Post().should.have.property('setUserId');
            new Post().setUserId.should.be.type('function');
            done();
        });

        it('должен установить значение при корректном id', function (done) {
            var newPost = new Post(),
                userId = new User().getId();

            newPost.setUserId(userId);
            newPost.getUserId().should.equal(userId);
            done();
        });
    });

    describe('#getId()', function () {
        it('должен быть', function (done) {
            new Post().should.have.property('getId');
            new Post().getId.should.be.type('function');
            done();
        });
    });

    describe('#save()', function () {

        it('Не должен быть сохранен без пользователя', function (done) {

            var newPost = new Post();

            newPost.setTitle('Новый пост');
            newPost.setText('Это текст нового охренительного поста!!!!');

            newPost.save(function (err, savedPost) {
                should.exists(err.errors.user);
                done();
            });

        });

        it('Не должен быть сохранен без заголовка', function (done) {

            var newPost = new Post();

            newPost.setText('Это текст нового охренительного поста!!!!');
            newPost.setUserId(g.user.getId());

            newPost.save(function (err, savedPost) {
                should.exists(err.errors.title);
                done();
            });

        });

        after(g.deletePost);

    });

};