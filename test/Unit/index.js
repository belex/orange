"use strict";

module.exports = function (app, g) {

    describe('User', function () { require('./user')(app, g); });
    describe('Problem', function () { require('./problem')(app, g); });
    describe('Contest', function () { require('./contest')(app, g); });
    describe('Post', function () { require('./post')(app, g); });

};