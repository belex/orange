#include <cstdio>
#include <iostream>
#include <new>
#include <fstream>
#include <string>
int main() {
std::ifstream ifs("problem.in", std::fstream::in);
std::ofstream ofs("problem.out", std::ofstream::out);

	int n, m, a;
	ifs >> n >> m >> a;

	long long ans = (n / a) + ( (n % a) ? 1 : 0);
	ans *= (m / a) + ( (m % a) ? 1 : 0);


	ofs << ans;
	return 0;

