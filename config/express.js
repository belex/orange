module.exports = function (app, config) {
    "use strict";

    var _ = require('underscore'),
        i18n = require('i18n'),
        express = require('express'),
        session = require('express-session'),
        favicon = require('serve-favicon'),
        cookieParser = require('cookie-parser'),
        bodyParser = require('body-parser'),
        moment = require('moment'),
        viewsFolder = require('path').normalize(__dirname + '/../app/views/'),
        ectRenderer = require('ect')({ cache: true, watch: true, root: viewsFolder, ext: '.ect'}),
        passport = require('passport'),
        MongoStore = require('connect-mongo')(session),
        winston = require('winston'),
        punycode = require('punycode'),
        errorHelper = require('mongoose-error-helper').errorHelper;

    i18n.configure({
        locales:['ru', 'en'],
        directory: __dirname + '/locales',
        defaultLocale: 'ru'
    });

    app.set('showStackError', true);

    // should be placed before express.static
    app.use(require('compression')({
        filter: function (req, res) {
            return (/json|text|javascript|css/).test(res.getHeader('Content-Type'));
        },
        level: 9
    }));

    app.use(express.static(config.root + '/public'));

    // set views path, template engine and default layout
    app.set('view engine', 'ect');
    app.set('views', viewsFolder);
    app.engine('.ect', ectRenderer.render);

    // cookieParser should be above session
    app.use(cookieParser());

    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ extended: false }));

    // parse application/json
    app.use(bodyParser.json());

    app.use(require('multer')({ dest: '/tmp/orange/uploads/'}));

//    app.use(favicon());
    app.use(require('morgan')('dev'));

    //локализация
    app.use(i18n.init);

    app.use(session({
        secret: config.secret,
        resave: true,
        saveUninitialized: true,
        store: new MongoStore({
            host: config.db.host,
            db: config.db.dbName,
            username: config.db.user,
            password: config.db.pass
        })
    }));

    app.use(require('connect-flash')());

    // Passport
    app.use(passport.initialize());
    app.use(passport.session());

    moment.locale('ru');

    /* Set custom variables */
    app.use(function(req, res, next) {

        var flashMessages = [],
            _render = res.render;

        /* Контейнер для переменных (custom vars) */
        req.cv = {};

        /* Работа с датами на клиенте */
        res.locals.moment = moment;

        /* Плюс андерскор */
        res.locals._ = _;

        /* Добавляем flash сообщения на клиент */
        _.each(req.flash(), function (arr, key) {
            _.each(arr, function (msg) {
                flashMessages.push({ type: key, message: msg });
            });
        });

        res.locals.flash = flashMessages;

        res.locals.user = req.user;

        res.handleDBError = function (err) {
            var errorMsg = '';
            _.each(errorHelper(err), function (msg) {
                req.flash('error', msg);
                errorMsg += msg;
            });

            res.redirect( req.headers.referer || '/' );

            return new Error(errorMsg);
        };

        res.render = function(view, options, callback) {

            options = options || {};

            if ( req.xhr ) {
                res.json(options);
            } else {
                _render.call(res, view, options, callback);
            }

        };

        res.render404 = function () {
            res.status(404).render('404');
        };

        next();

    });

    /* Биндим роуты */
    require('./routes')(app);

    app.use(function(err, req, res, next) {

        if ( err ) { console.log(err); }

        if ( err && err.errorCode ) {

            if ( err.errorCode === 401 ) {
                res.redirect('/login');
            } else {
                res.status(err.errorCode).render('404', { url: req.originalUrl });
            }

        } else {
            next();
        }

    });

    /* Если ничего не найдено - 404 */
    app.use(function(req, res, next) {
        res.status(404).render('404', { url: req.originalUrl });
    });

};