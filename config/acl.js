
"use strict";

// модуль является копией стандартного модуля ACL, только с переопределенным req.url = req.originalUrl
//var ACL = require('../app/classes/acl/index'),
var ACL = require('acl'),
    acl = new ACL(new ACL.memoryBackend()),
    User = require('mongoose').model('User');

acl.allow([
    {
        roles: ['admin'],
        allows: [
//            {resources:'blogs', permissions:['get','put','delete']},
            {
                resources: [
                    '/admin',
                    '/admin/problems',
                    '/admin/contests',
                    '/admin/posts',
                    '/admin/users',
                    '/cabinet',
                    'problem',
                    'contest',
                    'user',
                    'post'
                ],
                permissions: '*'
            }
        ]
    },
    {
        roles: ['user'],
        allows: [
            { resources: '/cabinet', permissions:['view'] },
            { resources: 'contest', permissions:['view', 'register'] },
            { resources: 'problem', permissions:['check'] }
        ]
    }

]);

acl.check = function (resource, actions) {

    var HttpError = function (errorCode, msg) {
        this.errorCode = errorCode;
        this.msg = msg;

        Error.captureStackTrace(this, arguments);
        Error.call(this, msg);
    };

    return function (req, res, next) {

        if ( ! req.user ) {
            next(new HttpError(401, 'User not authenticated'));
            return;
        }

        acl.isAllowed(req.user.getId(), resource, actions, function(err, allowed) {

            if ( err ) {
                next(new Error('Error checking permissions to access resource'));
            } else if ( allowed === false ) {
                next(new HttpError(403,'Insufficient permissions to access resource'));
            } else {
                next();
            }

        });

    };

};

// Добавляем  пользователей в группы
User.find().exec(function (err, users) {
    if ( err ) { throw err; }

    users.forEach(function (user) {
        acl.addUserRoles( user.getId(), user.admin ? 'admin' : 'user' );
    });

});

// Хардкод. Добавил себя в админы
//acl.addUserRoles( "544771409ad085de47a9ddcc", 'admin');


module.exports = acl;