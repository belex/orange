
"use strict";

/*
 |--------------------------------------------------------------------------------------------|
 | HTTP METHOD  |     ROUTE        |   ACTION  |                DESCRIPTION                   |
 |--------------------------------------------------------------------------------------------|
 | GET	        | /photos	         |   index	 |  display a list of all photos                |
 | GET	        | /photos/new	     |   new	   |  return an HTML form for creating a new photo|
 | POST 	      | /photos	         |   create  |  create a new photo                          |
 | GET	        | /photos/:id	     |   show	   |  display a specific photo                    |
 | GET	        | /photos/:id/edit |	 edit    |  return an HTML form for editing a photo     |
 | PATCH/PUT    | /photos/:id	     |   update  |  update a specific photo                     |
 | DELETE	      | /photos/:id	     |   destroy |	delete a specific photo                     |
 |--------------------------------------------------------------------------------------------|
 */

module.exports = function (app) {

    var acl = require('./acl'),
        router = require('express').Router();

    /* Клиентская часть */
    router.use('/', require('../app/controllers/user/index'));
    router.use('/', require('../app/controllers/user/authentication'));
    router.use('/attempts', require('../app/controllers/user/attempts'));
    router.use('/contests', require('../app/controllers/user/contests'));
    router.use('/problems', require('../app/controllers/user/problems'));
    router.use('/posts', require('../app/controllers/user/posts'));

    /* Личный кабинет */
    router.use('/cabinet', require('../app/controllers/cabinet/index'));

    /* Админ часть */
    router.use('/admin', acl.check('/admin', 'view'), require('../app/controllers/admin/index'));
    router.use('/admin/contests', acl.check('/admin/contests', 'view'), require('../app/controllers/admin/contests'));
    router.use('/admin/problems', acl.check('/admin/problems', 'view'), require('../app/controllers/admin/problems'));
    router.use('/admin/posts', acl.check('/admin/posts', 'view'), require('../app/controllers/admin/posts'));
    router.use('/admin/users', acl.check('/admin/users', 'view'), require('../app/controllers/admin/users'));

    app.use('/', router);

};
