
"use strict";

var env = require('../app/classes/registry').get('env');
var LocalStrategy = require('passport-local').Strategy;
var User = require('mongoose').model('User');

module.exports = function (passport, config) {

  // serialize sessions
  passport.serializeUser(function(user, done) {
      done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
      User.findById(id, function(err, user){

          if ( err ) {
              done(err);
          } else {
              done(null,user);
          }

      });
  });

  // use local strategy
  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'pass'
    },
    function(email, password, done) {

      User.authenticate(email, password, function (err, user) {
        if ( err ) {
          return done(err);
        }

        if ( ! user ) {
          return done(null, false, { message: 'Unknown user' });
        }

        return done(null, user);
      });
    }
  ));
};
