
module.exports = {

    development: {
        app: {
            name: 'Система проверки олимпиадных задач',
            shortname: 'SPB SUT Orange'
        },
        root: require('path').normalize(__dirname + '/..'),
        olympFolder: require('path').normalize(__dirname + '/../olymp'),
        db: {
            host: 'localhost',
            port: 27017,
            link: 'mongodb://localhost/',
            dbName: 'orange',
            options: {}
        },
        secret: 'fc9fbc5d42c2f4cdbdc3ac74184721c7',
        mail: {
            to: 'shalin.ivan@gmail.com, dan.nikonov@gmail.com',
            from: 'orange@spbgut.ru',
            host: 'b22-mt.itut.ru',
            port:  25,
            user: 'orange@spbgut.ru',
            pass: 'XR5jDu',
            tls: false
        },
        defaultPort: 8081
    },
    production: {
        app: {
            name: 'Система проверки олимпиадных задач',
            shortname: 'SPB SUT Orange'
        },
        root: require('path').normalize(__dirname + '/..'),
        olympFolder: require('path').normalize('/srv/app/olymp'),
        db: {
            host: '91.238.230.103',
            port: 27017,
            //link: 'mongodb://91.238.230.103:27017,172.16.66.201:27017,172.16.66.202:27017',
            link: 'mongodb://orange:orangemongodbadmin@91.238.230.103:27017/',
            dbName: 'orange',
            user: 'orange',
            pass: 'orangemongodbadmin',
            options: {
                //replicaSet: 'orange_replica_set'
            }
        },
        secret: 'fc9fbc5d42c2f4cdbdc3ac74184721c7',
        mail: {
            to: 'shalin.ivan@gmail.com, dan.nikonov@gmail.com',
            //    to: 'dan.nikonov@gmail.com',
            from: 'orange@spbgut.ru',
            host: 'b22-mt.itut.ru',
            port:  25,
            user: 'orange@spbgut.ru',
            pass: 'XR5jDu',
            tls: false
        },
        defaultPort: 8080
    },
    testing: {
        app: {
            name: 'Система проверки олимпиадных задач',
            shortname: 'SPB SUT Orange'
        },
        root: require('path').normalize(__dirname + '/..'),
        olympFolder: require('path').normalize('/tmp/orange/problems'),
        db: {
            host: 'localhost',
            port: 27017,
            link: 'mongodb://localhost/',
            dbName: 'orange_test',
            options: {}
        },
        secret: 'fc9fbc5d42c2f4cdbdc3ac74184721c7',
        mail: {
            to: 'shalin.ivan@gmail.com',
            from: 'vsevlestest@gmail.com',
            host: 'smtp.gmail.com',
            port:  465,
            secure: true,
            user: 'vsevlestest@gmail.com',
            pass: 'vDez5Semqt',
            tls: false
        },
        defaultPort: 8082
    }
};
