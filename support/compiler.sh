#!/bin/bash
GCC_FLAGS=-O2
SRC="/tmp/exec/$1"
OUT="/tmp/exec/a.out"
EXEC="g++ $GCC_FLAGS $SRC -o $OUT"
chroot --userspec checker:checker /sandbox/1/wheezy/ $EXEC > /dev/null 2>&1
echo $?
