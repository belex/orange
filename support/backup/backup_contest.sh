#!/bin/bash
DATE=`date +%F`
DATETIME=`date +%F_%H%M`
FOLDER=/home/backup/$DATE
if [ ! -d $FOLDER ]; then
  mkdir -p $FOLDER
fi

cd $FOLDER
if [ ! -d "$FOLDER/contest" ]; then
  mkdir contest
fi

cd contest
mongodump --host 91.238.230.103 --username orange --password orangemongodb -d orange -o $DATETIME

# mongorestore --dbpath /var/lib/mongodb -d orange /home/backup/orange
