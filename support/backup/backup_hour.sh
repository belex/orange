#!/bin/bash
DATE=`date +%F`
DATETIME=`date +%F_%H%M`
FOLDER=/home/backup/$DATE
if [ ! -d $FOLDER ]; then
  mkdir -p $FOLDER
fi

cd $FOLDER
if [ ! -d "$FOLDER/hour" ]; then
  mkdir hour
fi

cd hour
mongodump --host 91.238.230.103 --username orange --password orangemongodb -d orange -o orange

tar cjf mongo.tar.gz orange
rm -rf orange

tar cjf git.tar.gz /srv/repos
tar cjf hour_backup_$DATETIME.tar.gz {git,mongo}.tar.gz

rm {git,mongo}.tar.gz

# mongorestore --dbpath /var/lib/mongodb -d orange /home/backup/orange
