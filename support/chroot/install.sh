#!/bin/bash

[ -f files/vars.sh ] && . files/vars.sh || exit 0

msg() {
  echo "Installer: "$1
}

add_user() {
  msg "Add user"
  ALLOW=1
  if id -u $USERNAME > /dev/null 2>&1; then
    msg "User" $USERNAME "already exists!"
    ALLOW=0
  fi
  if id -g $GROUPNAME > /dev/null 2>&1; then
    msg "Group $GROUPNAME already exists!"
    ALLOW=0
  fi
  if [ "$ALLOW" -eq 0 ]; then
    exit 0
  else
    [ -d /tmp/$USERNAME ] && rm -rf /tmp/$USERNAME
    groupadd -g $CHECKER_GID $GROUPNAME 
    useradd -mb /tmp -g $GROUPNAME -u $CHECKER_UID -s /bin/bash $USERNAME
    usermod -aG $GROUPNAME node
  fi
}
install_dist() {
  msg "Install chroot"
  ALLOW=1
  if ! dpkg -l | grep debootstrap >/dev/null 2>&1; then
    msg "Package debootstrap not installed!"
    ALLOW=0
  fi

  if [ "$ALLOW" -eq 0 ]; then
    exit 0
  else
    if mount | grep $SANDBOX > /dev/null 2>&1; then
      msg "Sandbox mounted!"
      [ ! -d $CHROOT ] && mkdir -p $CHROOT
      if debootstrap $DIST_NAME $CHROOT http://http.debian.net/debian; then
        msg "Installing g++"
        chroot $CHROOT aptitude install g++ -y
      else
        msgo "Fail to install chroot!"
        rm -rf $CHROOT
        exit 0;
      fi
    else
      msg "Sandbox not mounted!"
      exit 0
    fi
  fi
}

configure() {
  msg "Configure!"
  cp $FILES_PATH"/limits.conf" $CHROOT"/etc/security/limits.conf"
  cp $FILES_PATH"/su" $CHROOT"/etc/pam.d/su"
  cp $FILES_PATH"/login" $CHROOT"/etc/pam.d/login"
  cp $FILES_PATH"/c_prepare.sh" $CHROOT"/tmp/c_prepare.sh"
  cp $FILES_PATH"/env.sh" $CHROOT"/tmp/env.sh"
  # prepare chroot environment
  chroot $CHROOT bash /tmp/c_prepare.sh $USERNAME $GROUPNAME $CHECKER_UID
  
  mkdir -p $COMPILE_PATH
  mkdir -p $HISTORY_PATH
  
  # echo "mount -o bind /dev $CHROOT/dev # $CHROOT" >> /etc/fstab
  echo "/sys $C_SYS sysfs bind 0 0" >> /etc/fstab
  echo "/proc $C_PROC proc bind 0 0" >> /etc/fstab
  echo "$C_EXEC_PATH $COMPILE_PATH none bind 0 0" >> /etc/fstab
  echo "$C_HISTORY_PATH $HISTORY_PATH none bind 0 0" >> /etc/fstab
  mount -a

#  mount -o bind $C_EXEC_PATH $COMPILE_PATH
#  mount -o bind $C_HISTORY_PATH $HISTORY_PATH
}

# main function
add_user
install_dist
configure
