#!/bin/bash

IN=$1
TIME=$2
MEMORY=$3

USERNAME=checker
GROUPNAME=checker
EXEC_PATH='/tmp/exec'
HISTORY_PATH='/tmp/history'
ulimit -c 0		#core file size       
ulimit -d unlimited	#data seg size        
ulimit -e 0		#scheduling priority  
ulimit -f unlimited	#file size            
ulimit -i 200		#pending signals        #changed
ulimit -l 64		#max locked memory    
ulimit -m $MEMORY	#max memory size    	#changed 
ulimit -n 20		#open files             #changed 
# ulimit -p 512		#pipe size            
ulimit -q 819200	#POSIX message queues 
ulimit -r 0		#real-time priority   
ulimit -s 8192		#stack size           
ulimit -t 5		#cpu time		#chenged
ulimit -u 10		#max user processes  	#changed
ulimit -v $MEMORY	#virtual memory       
ulimit -x 100		#file locks           

# ulimit -a
cd $EXEC_PATH
# NAME=$1.cpp
EXEC=a.out
#GCC_FLAGS=-O2
#g++ $GCC_FLAGS $NAME -o $EXEC
timeout $TIME ./$EXEC $IN > /dev/null 2>&1
CODE=$?
case $CODE in
  0)
  echo -n "OK"
  ;;
  124)
  echo -n "TL" # time limit
  ;;
  134)
  echo -n "RE" # runtime error
  ;;
  139)
  echo -n "SF" # segmentation fault
  ;;
  *)
  echo -n "UE" # unknown error
  ;;
esac

# mv $NAME $HISTORY/`date +%F`_$NAME
#rm $EXEC
#echo $CODE

