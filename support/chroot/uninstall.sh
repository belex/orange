#!/bin/bash
# uninstall chroot environment
[ -f files/vars.sh ] && . files/vars.sh || exit 0

msg() {
  echo "Uninstaller: "$1
}

del_user() {
  msg "Delete user"
  gpasswd -d node $GROUPNAME
  userdel $USERNAME > /dev/null 2>&1
  groupdel $GROUPNAME > /dev/null 2>&1
}

uninstall_dist() {
  msg "Uninstall chroot"
  [ -d $CHROOT ] && rm -rf $CHROOT
  [ -d $WORKER_PATH ] && rm -rf $WORKER_PATH
}

change_fstab() {
 msg "Change fstab!"
 BS_PATH=$(echo $CHROOT | sed 's/\//\\\//g')
 sed "/`echo $BS_PATH`/d" /etc/fstab > /etc/fstab.new
 mv /etc/fstab.new /etc/fstab	
 msg "Remount from fstab!"
 mount -a
}

umount_chroot() {
  msg "Umount filesystems!"
  if mount | grep $C_SYS > /dev/null 2>&1; then 
    msg "Umount $C_SYS"
    umount $C_SYS 
  else 
    msg "$C_SYS not mounted!"
  fi

  if mount | grep $C_PROC > /dev/null 2>&1; then
    msg "Umount $C_PROC"
    umount $C_PROC 
  else
    msg "$C_PROC not mounted!"
  fi
   
  if mount | grep $COMPILE_PATH > /dev/null 2>&1; then
    msg "Umount $COMPILE_PATH"
    umount $COMPILE_PATH 
  else 
    msg "$COMPILE_PATH not mounted!"
  fi

  if mount | grep $HISTORY_PATH > /dev/null 2>&1; then
    msg "Umount $HISTORY_PATH"
    umount $HISTORY_PATH 
  else 
    msg "$HISTORY_PATH not mounted!"
  fi

}
umount_chroot
change_fstab
#uninstall_dist
del_user
