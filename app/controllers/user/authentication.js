
"use strict";

var _ = require('underscore'),
    express = require('express'),
    mailer = require('../../classes/mailer/mailer'),
    passport = require('passport'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    router = express.Router();

router.get('/register/sendemail', function (req, res) {
    res.render('user/register/sendemail');
});

router.post('/register/sendemail', function (req, res) {
    console.log(req.body.email);
});

router.get('/register', function (req, res) {
    res.render('reg');
});

router.post('/register', function (req, res) {

    var params = req.body;

    User.findOne({ email: new RegExp(params.email) }, function (err, user) {
        if ( err ) { throw err; }

        if ( user ) {

            req.flash('error', 'Пользователь с таким email уже существует!');
            res.redirect('/register');

        } else {

            user = new User();

            user.setFname(req.body.fname);
            user.setLname(req.body.lname);
            user.setGroup(req.body.group);
            user.setEmail(req.body.email);
            user.setPass(req.body.pass);

            user.save(function (err, user) {
                if ( err ) { throw err; }

                if ( user ) {

                    req.flash('info', 'Пользователь успешно зарегистрирован!');
                    res.redirect('/');
                    mailer.send('registration', { user: user });

                }

            });

        }

    });

});

router.get('/login', function (req, res) {
    res.render('login');
});

router.post('/login', passport.authenticate('local', { failureRedirect: '/login' }), function (req, res) {
    res.redirect('/');
});

router.get('/logout', function (req, res) {

    req.logout();

    if ( req.headers.referer ) {
        res.redirect(req.headers.referer);
    } else {
        res.redirect('/');
    }
});

module.exports = router;