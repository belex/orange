
"use strict";

var _        = require('underscore'),
    moment   = require('moment'),
    mongoose = require('mongoose'),
    Post     = mongoose.model('Post'),
    Contest  = mongoose.model('Contest'),
    router   = require('express').Router();

router.get('/', function (req, res) {

    Post.find().sort('-dateCreated').exec(function (err, posts) {
        if ( err ) { throw err; }

        Contest.find({ allowRegister: true, end: {$gt: Date.now()} }).sort('-start').exec(function (err, contests) {
            if ( err ) { throw err; }

//            data.countdown = moment.duration(contest.start - Date.now())
//                .subtract(moment.duration({days: 1, hours: 4}))
//                .asMilliseconds();
//
//            data.allowRegister = !!(req.user && ! _.find(contest.users, function(user) {
//                return String(user) === req.user.getId();
//            }));

            res.render('user/main', {
                posts: posts,
                contests: contests
            });

        });
    });





});

module.exports = router;