
"use strict";

var fs          = require('fs-extra'),
    acl         = require('../../../config/acl'),
    logger      = require('winston'),
    mongoose    = require('mongoose'),
    Problem     = mongoose.model('Problem'),
    Attempt     = mongoose.model('Attempt'),
    router      = require('express').Router(),
    Checker     = require('../../checker/checker');

router.get('/', function(req, res) {

    Problem.find({ active: true }).sort({dateCreated: 1}).populate('attempts').exec(function(err, problems) {
        if ( err ) { throw err; }

        res.render('user/problems/index', {
            problems: problems
        });
    });

});

router.get('/:id', function(req, res) {

    Problem.findById(req.params.id, function(err, problem) {
        if ( err ) { req.handleDBError(err); }

        if ( req.user ) {
            Attempt.find({ problem: problem.getId(), user: req.user.getId() }).sort('-dateCreated').exec(function (err, attempts) {
                if ( err ) { req.handleDBError(err); }

                res.render('user/problems/show', {
                    problem: problem,
                    attempts: attempts
                });

            });

        } else {
            res.render('user/problems/show', { problem: problem });
        }


    });

});

router.post('/:id/check', acl.check('problem', 'check'), function(req, res) {

    console.log('file: ' + req.files.source.path);

    Problem.findById(req.params.id, function (err, problem) {
        if ( err ) { throw err; }

        logger.info('Попытка решить задачу ' + problem.getName() + ' пользователем ' + req.user.getFullName());

        Checker.check(req.user, problem, require('path').normalize('/' + req.files.source.path), function (err) {
            if ( err ) { throw err; }

            console.log(req.files.source.path);

            res.redirect(req.headers.referer);

        });

    });


//    if ( req.body.contest ) {
//        res.redirect('/contests/' + req.body.contest);
//    } else {
//        res.redirect('/attempts');
//    }


});

module.exports = router;