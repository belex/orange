
"use strict";

var mongoose = require('mongoose'),
    Post     = mongoose.model('Post'),
    router   = require('express').Router();

router.get('/:id', function(req, res) {

    Post.findById(req.params.id, function(err, post) {
        if ( err ) { req.handleDBError(err); }

        res.render('user/posts/show', {
            post: post
        });
    });

});

module.exports = router;