
"use strict";

var _           = require('underscore'),
    i18n        = require('i18n'),
    express     = require('express'),
    mongoose    = require('mongoose'),
    Problem     = mongoose.model('Problem'),
    Attempt     = mongoose.model('Attempt'),
    router      = express.Router();


/* ========================================================================
 * Попытки
 * ======================================================================== */

router.get('/', function (req, res) {

    var user = req.user,
        page = parseInt(req.params.page, 10) || 1,
        perPage = 20,
        problemsQuery = Problem.find();

    /* Показываем юзерам только попытки к опубликованным задачам */
    if ( ! (user && user.admin) ) {
        problemsQuery.where('active').equals(true); //TODO: пока не готов показ попыток только одного чела показываем все попытки
    }

    problemsQuery.exec(function (err, problems) {
        if ( err ) { throw err; }

        var query = {
            problem: {
                $in: _.pluck(problems, '_id')
            }
        };

        Attempt.count(query, function (err, count) {
            Attempt.find(query)
                .sort('-dateCreated')
                .skip((page - 1) * perPage)
                .limit(perPage)
                .populate('user', 'lname fname')
                .populate('problem', 'name active')
                .exec(function (err, attempts) {
                    if ( err ) { throw err; }

                    var data = {
                        attempts: attempts,
                        page: page,
                        pageCount: parseInt(count / perPage, 10) + 1
                    };

                    res.render('user/attempts/list', data);

                });
        });

    });

});

module.exports = router;
