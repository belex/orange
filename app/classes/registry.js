/**
 * User: shalin
 * Date: 20.10.13
 * Time: 1:26
 * Немного тестированная реализация паттерна Реестр.
 */

"use strict";

var _ = require('underscore');

/**
 * Класс реализующий паттерн Реестр
 * @constructor
 */
function RegistryClass () {

  /** @private */
  var _storage = {};

  /** @private */
  var _fabric = {};

  /**
   * Добавляет объект в реестр
   *
   * @public
   * @param {String} key
   * @param value
   * @param options
   */
  this.set = function (key, value, options) {
    if ( arguments.length === 3 ) {
      _fabric[key] = [value, options];
    }
    _storage[key] = value;
  };

  /**
   * Удаляет объект из реестра
   *
   * @public
   * @param key
   * @param isFabric
   */
  this.remove = function (key, isFabric) {
    if ( isFabric ) {
      delete _fabric[key];
    } else {
      delete _storage[key];
    }
  };

  /**
   * Возвращает объект из реестра
   *
   * @public
   * @param key
   * @param options
   * @returns {*}
   */
  this.get = function (key, options) {
    if ( arguments.length === 2 ) {
      if ( _fabric[key] ) {
        var fabric = _fabric[key];
        var create = fabric[0];
        var params = _.extend({}, fabric[1], options);

        return create(params);
      }
      return _storage[key] || options;
    }
    return _storage[key];
  };
}

module.exports = new RegistryClass();