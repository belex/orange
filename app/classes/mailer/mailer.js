/**
 * User: shalin
 * Date: 08.11.13
 * Time: 1:12
 * Модуль шлимыла. Реализует интерфейс наружу с указанием типа отсылаемого сообщения
 */

"use strict";

var nodemailer = require('nodemailer');
var mongoose = require('mongoose');
var env = require('./../registry').get('env');
var config = require('../../../config/config')[env];
var logger = require('winston');
var ECT = require('ect');
var renderer = ECT({ root : __dirname + '/views' });
var User = require('mongoose').model('User');
var moment = require('moment');


/**
 * Конструктор класса Mailer
 * @constructor
 */
var Mailer = function MailerClass() {

  /** @private Почта с которой будут слаться сообщения */
  var from = config.mail.from;

  /** @private Инициализация транспортного канала для передачи сообщений */
  var smtpTransport = nodemailer.createTransport('SMTP', {
    host: config.mail.host,
    port: config.mail.port,
    tls: config.mail.tls,
    auth: {
      user: config.mail.user,
      pass: config.mail.pass
    }
  });

  /**
   * Шлимыло. Принимает параметры и посылает сообщение по транспорту.
   * @param params
   */
  this.sendMail = function (params) {
    var mailOptions = {
      from: from,
      to: params.to,
      subject: params.subj,
      html: params.html
    };

    smtpTransport.sendMail(mailOptions, function(err, res) {
      if ( err ) {
        logger.error(err);
      } else {
        logger.info('Message sent: ' + res.message);
      }
    });
  };

  /**
   * Метод тестирующий другие методы отправки сообщений и мейлер в целом.
   * Принимает имя метода для тестирования, если не указан, то просто посылает
   * сообщение тестируя транспорт.
   * @param {String} action
   */
  this.test = function (action) {

    var self = this;


    var data = {
      to: 'shalin.ivan@gmail.com',
      subj: 'Тестовая рассылка',
      html: '<p>Будь хорошим мальчиком, не говнокодь!</p>'
    };

    switch (action) {
      case 'test':
        this.test();
        break;
      case 'registration':
        mongoose.model('User').findOne(function (err, user) {
          data.user = user;
          self.send(action, data);
        });
        break;
      case 'contestRegister':
        mongoose.model('User').findOne(function (err, user) {
          mongoose.model('Contest').findOne(function (err, contest) {
            data.userId = user._id;
            data.contest = contest;
            self.send(action, data);
          });
        });
        break;
      default:
        this.test();
    }

    if ( action ) {
      var user = require('mongoose').model('User').findOne(function (err, user) {
        data.user = user;
        self.send(action, data);
      });
    }
  };

  /**
   * Метод, отвечающий за формирование сообщения о регистрации в системе
   * @param params
   */
  this.registration = function (params) {

    var data = {
      user: params.user
    };

    var html = renderer.render('registration.ect', data);

    var mailOptions = {
      from: config.mail.from,
      to: params.to || params.user.email,
      subj: 'Регистрация в истеме проверки олимпиадных задач',
      html: html,
      charset: 'utf-8'
    };

    this.sendMail(mailOptions);
  };

  this.contestRegister = function (params) {

    var self = this;

    if ( params.userId ) {
      User.findById(params.userId, function (err, user) {
        if ( err ) {
          throw err;
        }

        var data = {
          user: user,
          contest: params.contest,
          moment: moment
        };

        var html = renderer.render('contest_registration.ect', data);

        var mailOptions = {
          from: config.mail.from,
          to: params.to || user.email,
          subj: 'Регистрация на контест',
          html: html,
          charset: 'utf-8'
        };

        self.sendMail(mailOptions);
      });
    }
  };

  /**
   * Метод интерфейс. Принимает тип посылаемого сообщения (action) и параметры к нему
   * @param action
   * @param params
   */
  this.send = function (action, params) {

    this[action](params);
//    switch (action) {
//      case 'test':
//        this.testMail(params);
//        break;
//      case 'registration':
//        this.registration(params);
//        break;
//      case 'contestRegister':
//        this.registration(params);
//        break;
//    }
  };
};

module.exports = new Mailer();