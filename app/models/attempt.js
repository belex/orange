
"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    modelName = 'Attempt';


/* ========================================================================
 * Схема в БД
 * ======================================================================== */

var schema = new Schema({
        problem: { type: Schema.Types.ObjectId, ref: 'Problem'},
        user: { type: Schema.Types.ObjectId, ref: 'User'},
        test: { type: Number, default: 0 },
        ans: { type: Number },
        source: { type: String },
        dateCreated: { type: Date, default: Date.now }
    }, {
        safe: true,
        strict: true
    });


// походу для вывода на клиент и к этому модулю не имеет отношения, нужно вынести во вьюху
//_self.setResult = function (attempts) {
//    _.each(attempts, function(attempt) {
//        attempt.result = i18n.__('code ' + attempt.ans);
//
//        switch (attempt.ans) {
//            case 2:
//            case 3:
//            case 4:
//            case 6:
//                attempt.result = attempt.result + ' ' + i18n.__('on test') + ' ' + attempt.test;
//                break;
//        }
//    });
//
//    return attempts;
//
//};

/* ========================================================================
 * Статические методы
 * ======================================================================== */

//categorySchema.statics.create = function (name, siteId, cb) {
//    var newCategory = new this();
//
//    newCategory.setName(name);
//    newCategory.setSiteId(siteId);
//
//    newCategory.save(cb);
//};

mongoose.model(modelName, schema);




