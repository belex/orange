
"use strict";

var moment = require('moment'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    modelName = 'Contest',
    schema = new Schema({
        name: { type: String },
        text: { type: String, default: '' },
        start: { type: Date },
        end: { type: Date },
        users: [{ type: Schema.Types.ObjectId, ref: 'User', default: [] }],
        problems: [{ type: Schema.Types.ObjectId, ref: 'Problem', default: [] }],
        dateCreated: { type: Date, default: Date.now },
        allowRegister: { type: Boolean, default: false }
    }, {
        safe: true,
        strict: true
    });


/* ========================================================================
 * Методы
 * ======================================================================== */

schema.methods.getId = function () {
    return this._id;
};

schema.methods.getName = function () {
    return this.name;
};

schema.methods.setName = function (name) {
    this.name = String(name).trim();
};

schema.methods.getText = function () {
    return this.text;
};

schema.methods.setText = function (text) {
    this.text = String(text).trim();
};

schema.methods.getStart = function () {
    return this.start;
};

schema.methods.setStart = function (timeStart) {
    this.start = moment(timeStart, 'DD.MM.YYYY HH:mm:ss').valueOf();
};

schema.methods.getEnd = function () {
    return this.end;
};

schema.methods.setEnd = function (timeEnd) {
    this.end = moment(timeEnd, 'DD.MM.YYYY HH:mm:ss').valueOf();
};

schema.methods.getProblems = function () {
    return this.problems;
};

schema.methods.setProblems = function (problems) {
    this.problems = problems || [];
};

schema.methods.getUsers = function () {
    return this.users;
};

schema.methods.setUsers = function (users) {
    this.users = users || [];
};

schema.methods.setAllowRegister = function (allow) {
   this.allowRegister = !!allow;
};

mongoose.model(modelName, schema);

